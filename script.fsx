open System
open System.Threading

let createTimer timeInterval eventHandler = 
    let timer = new Timers.Timer(float timeInterval)
    timer.AutoReset<-true

    timer.Elapsed.Add eventHandler

    async{
        timer.Start()
        do!Async.Sleep 5000
        timer.Stop()
    }

let basicHandler _ = printfn"tick %A" DateTime.Now
let basicTimer1 = createTimer 1000 basicHandler

Async.RunSynchronously basicTimer1



let createTimerAndObservable timeInterval =
    let timer = new Timers.Timer(float timeInterval)
    timer.AutoReset<-true

    let observable = timer.Elapsed

    // let task = task{
    //     timer.Start()
    //     do! Async.Sleep 5000
    //     timer.Stop()
    // }

    let task = async{
        timer.Start()
        do! Async.Sleep 5000
        timer.Stop()
    }
    (task,observable)

let basicTimer2 , timerEventStream = createTimerAndObservable 1000

timerEventStream|>Observable.subscribe (fun _->printfn"Tick %A" DateTime.Now)

// basicTimer2.RunSynchronously()
Async.RunSynchronously basicTimer2

type ImperativeCounter() =
    let mutable count = 0
    member this.handleEvent _ =
        count<-count+1
        printfn"timer ticked with count %i"count

let handler = new ImperativeCounter()
let timerCount1 = createTimer 500 handler.handleEvent

Async.RunSynchronously timerCount1


let timerCount2, timerEventStream2 = createTimerAndObservable 500
timerEventStream2|>Observable.scan (fun count _->count+1) 0|>Observable.subscribe(fun count->printfn"timer ticked func way: %i"count)
Async.RunSynchronously timerCount2