module GiraffeLearning.App

open System
open System.IO
open System.Net.Http
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe

// ---------------------------------
// Models
// ---------------------------------

type Message =
    {
        Text : string
    }

// ---------------------------------
// Views
// ---------------------------------

module Views =
    open Giraffe.ViewEngine

    let layout (content: XmlNode list) =
        html [] [
            head [] [
                title []  [ encodedText "GiraffeLearning" ]
                link [ _rel  "stylesheet"
                       _type "text/css"
                       _href "/main.css" ]
            ]
            body [] content
        ]

    let partial () =
        h1 [] [ encodedText "GiraffeLearning" ]

    let index (model : Message) =
        [
            partial()
            p [] [ encodedText model.Text ]
        ] |> layout

// ---------------------------------
// Web app
// ---------------------------------

let indexHandler (name : string) =
    let greetings = sprintf "Hello %s, from Giraffe!" name
    let model     = { Text = greetings }
    let view      = Views.index model
    htmlView view

let app:HttpHandler = compose (route "/") (Successful.OK "hello world")
let app2:HttpHandler = route "/" >=> Successful.OK "hello world"
let sayHelloWorld :HttpHandler = text "Hello world"
let sayHelloWorld2 (name:string):HttpHandler =
    let greetings = sprintf "Hello world, %s" name
    text greetings
let sayHelloWorld3 :HttpHandler =
    fun (next) (ctx) ->
        let name = ctx.TryGetQueryStringValue"name"|>Option.defaultValue"Giraffe"
        let greetings = sprintf "Hello world, %s" name
        text greetings next ctx
    
type Person = {name:string}
let sayHelloWorld5 :HttpHandler =
    fun (next) (ctx) ->
        task{
            let! person = ctx.BindJsonAsync<Person>()
            let greetings = sprintf "Hello world, %s" person.name
            return!text greetings next ctx
            }
        
let handlerWithoutLogging : HttpHandler = handleContext(fun ctx ->
        let logger = ctx.GetService<ILogger>()
        logger.LogInformation("From the context")
        ctx.WriteTextAsync ""
        )
let handlerWithoutLogging2 : HttpHandler = handleContext(fun ctx->task{
        let logger = ctx.GetService<ILogger>()
        logger.LogInformation("From the task context")
        return! ctx.WriteTextAsync "Done working"
    })
let time() = System.DateTime.Now.ToString()
let webApp =
    choose [
        GET >=> choose [
                route "/" >=> indexHandler "world"
                route "/route"
                route "/normal" >=> text (time())
                route "/warbler" >=> warbler (fun _->text(time()))
                route "/compose" >=> setHttpHeader "X-Foo" "Bar" >=> setStatusCode 200 >=> setBodyFromString "Hello World"
                
                routef "/hello/%s" indexHandler
                route "/sayhello" >=> sayHelloWorld
                route "/sayhello2" >=> sayHelloWorld2 "John"
                route "/sayhello3" >=> sayHelloWorld3
                route "/sayhello5" >=> sayHelloWorld5
                
                route "/withoutlogging" >=> handlerWithoutLogging
                route "/withoutlogging2" >=> handlerWithoutLogging2
            ]
        setStatusCode 404 >=> text "Not Found" ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

// ---------------------------------
// Config and Main
// ---------------------------------

let configureCors (builder : CorsPolicyBuilder) =
    builder
        .WithOrigins(
            "http://localhost:5000",
            "https://localhost:5001")
       .AllowAnyMethod()
       .AllowAnyHeader()
       |> ignore

let configureApp (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    (match env.IsDevelopment() with
    | true  ->
        app.UseDeveloperExceptionPage()
    | false ->
        app .UseGiraffeErrorHandler(errorHandler)
            .UseHttpsRedirection())
        .UseCors(configureCors)
        .UseStaticFiles()
        .UseGiraffe(webApp)

let configureServices (services : IServiceCollection) =
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main args =
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot     = Path.Combine(contentRoot, "WebRoot")
    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .UseContentRoot(contentRoot)
                    .UseWebRoot(webRoot)
                    .Configure(Action<IApplicationBuilder> configureApp)
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    |> ignore)
        .Build()
        .Run()
    0