type Contact={
  Firstname:string
  Middleinitial:string
  Lastname:string

  Email:string
  IsEmailVerified:bool
}

module CardGame=
  type Suit = Club|Diamond|Spade|Heart
  type Rank = Two|Three|Four|Five|Six|Seven|Eight|Nine|Ten|Jack|Queen|King|Ace
  type Card = Suit*Rank
  type Hand = Card list
  type Deck = Card list
  type Player = {Name:string; Players:Player list}
  type Deal = Deck->(Deck*Card)
  type PickupCard = (Hand*Card)->Hand

type PaymentMethod = 
  |Cash
  |Cheque of int//ChequeNumber
  |Card of int*int//CardType*CardNumber

let printPayment method = 
  match method with
  |Cash->printfn"Cash"
  |Cheque chequeNumber->printfn"Cheque %A"chequeNumber
  |Card(cardType,cardNumber)->printfn"Card %A %A" cardType cardNumber

let cash1 = Cash
printPayment cash1


type OptionalString =  
  |SomeString of string
  |Nothing

type OptionalInt =
  |SomeInt of int
  |Nothing

type OptionalBool = 
  |SomeBool of bool
  |Nothing

// type Option<'T> = 
//   |Some of'T
//   |None



type EmailAddress = EmailAddress of string
type PhoneNumber = PhoneNumber of string

type CustomerId = CustomerId of int
type OrderId = OrderId of int


// let createEmailAddress (s:string) = 
//   if Regex.IsMatch(s,@"^S+@\S+\.\S+$")
//     then Some (EmailAddress s)
//     else None

type String50 = string

let createString50 (s:string) = 
  if s.Length<=50
    then Some (String50 s)
    else None

type OrderLineQty = OrderLineQty of int

let createOrderLineQty qty =
  if qty>0&&qty<=99
    then Some (OrderLineQty qty)
    else None

type PersonalName = 
  {
    FirstName: String50
    MiddleInitial: String50 option
    LastName: String50
  }


type Cont = {
  Name: PersonalName
  Email: EmailAddress
}