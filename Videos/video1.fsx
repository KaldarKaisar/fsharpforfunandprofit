let printList anAction aList=
  for i in aList do
    anAction i

let hello = printfn "Hello mrs %s"

hello "John"

let names = ["Alice";"Bob";"Scott"]

List.iter hello names
names|>List.iter hello

let add1 = (+)1
let equals2 = (=) 2
printfn "%d"<|add1 3
printfn "%A"<|equals2 2

[1..100]|>List.map add1|>List.filter equals2|>printfn"%A"

[1..4]|>List.reduce (+)|>printfn"%d"
[1..5]|>List.reduce (*)|>printfn"%d"
let add a b=a+b
[1..4000000]|>List.reduce add|>printfn"%d"

Some 1|>Option.map (fun a->a+42)|>printfn"%A"