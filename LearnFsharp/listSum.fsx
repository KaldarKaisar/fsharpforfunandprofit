// define the square function
let square x = x * x

// define the sumOfSquares function
let sumOfSquares n =
   [1..n] |> List.map square |> List.sum

// try it
sumOfSquares 100


let sumOfSquares n =
   [1..n]
   |> List.map square
   |> List.sum

   // define the square function
let squareF x = x * x

// define the sumOfSquares function
let sumOfSquaresF n =
   [1.0 .. n] |> List.map squareF |> List.sum  // "1.0" is a float

sumOfSquaresF 100.0


// define the square function
let square x = x * x

// test
let s2 = square 2
let s3 = square 3
let s4 = square 4