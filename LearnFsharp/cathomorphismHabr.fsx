let a = sprintf"Hello %s"

let hiJohn = a "John"

printfn "%s"hiJohn


[1..10]|>List.fold (+) 0|>printfn"%A"
[1..1000000]|>List.fold (+) 0|>printfn"%A"

let rec sum list =
    match list with
    |[]->0
    |head::tail->head+sum tail

[1..10]|>List.sum|>printfn"%A"
// [1..1000000]|>List.sum|>printfn"%A"

[1..10]|>sum|>printfn"%A"
// [1..1000000]|>sum|>printfn"%A"

let sum_tail_rec list =
    let rec loop list acc =
        match list with
        |[]->acc
        |head::tail->loop tail (acc+head)
    loop list 0

[1..10]|>sum_tail_rec|>printfn"%A"
[1..1000000]|>sum_tail_rec|>printfn"%A"

let length list =
    let rec loop list acc =
        match list with
        |[]->acc
        |head::tail->loop tail (acc+1)
    loop list 0



[1..10]|>length|>printfn"%A"
[1..1000000]|>length|>printfn"%A"
[1..1000000]|>List.length|>printfn"%A"

let rec loop list acc =
    match list with
    |[]->acc
    |head::tail->loop tail (acc+head)
loop [1..10] 0|>printfn"%A"
loop [1..1000000] 0|>printfn"%A"

