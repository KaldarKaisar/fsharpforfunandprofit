
type USAddres = {Street:string; City:string; State:string; Zip:string}
type UKAddres = {Street:string; Town:string; PostCode:string}

type Address = 
    |US of USAddres
    |UK of UKAddres

type Person = {Name:string; Address:Address}

let alice = {
    Name="Alice"
    Address=US {
        Street="123 Main"
        City="LA"
        State="ZA"
        Zip="91201"
    }
}

let bob = {
    Name="Bob"
    Address=UK {
        Street="221b Baker St"
        Town="London"
        PostCode="NW1 6XE"
    }
}

printfn "Alice is %A" alice
printfn "Bob is %A" bob


type PersonalName = {FirstName:string; LastName:string}
let alice1 = {FirstName="Alice"; LastName="Adams"}
let alice2 = {FirstName="Alice"; LastName="Adams"}
let bob1 = {FirstName="Bob"; LastName="Bishop"}

printfn"%b"<|(alice1=alice2)
printfn"%b"<|(alice2=bob1)

type Suit = Club|Diamond|Spade|Heart
type Rank = Two|Three|Four|Five|Six|Seven|Eight|Nine|Ten|Jack|Queen|King|Ace

let compareCard card1 card2 =
    if card1>card2
    then printfn"%A is greater than %A" card1 card2
    else printfn"%A is greater than %A" card2 card1

let aceHearts = Heart, Ace
let twoHearts = Heart, Two
let aceSpades = Spade, Ace

compareCard aceHearts twoHearts
compareCard aceSpades aceHearts

let hand = [Club,Ace;Heart,Three;Heart,Ace;Spade,Jack;Diamond,Two;Diamond,Ace]

printfn"%A" hand
List.sort hand|>printfn"%A is sorted"
List.min hand|>printfn"%A is min"
List.max hand|>printfn"%A is max"