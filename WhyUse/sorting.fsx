let rec quicksort2 = function
   | [] -> []
   | first::rest ->
        let smaller,larger = List.partition ((>=) first) rest
        List.concat [quicksort2 smaller; [first]; quicksort2 larger]

// test code
printfn "%A" (quicksort2 [1;5;23;18;9;1;3])

let rec quicksort1 list=
    match list with
    |[]->[]
    |firstElem::otherElements->
        let smallerElements=
            otherElements|>List.filter(fun e->e<firstElem)|>quicksort1
        let largerElements=  
            otherElements
            |>List.filter(fun e->e>=firstElem)|>quicksort1
        List.concat [smallerElements;[firstElem]; largerElements]
printfn "%A" (quicksort1 [-1;-2;-5;1;5;23;18;9;1;3])
