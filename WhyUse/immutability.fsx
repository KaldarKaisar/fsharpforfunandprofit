let list = [1..4]
let list1 = 0::list
let list2 = list1.Tail

System.Object.ReferenceEquals(list,list2)|>printfn"%A"
list|>printfn"%A"
list1|>printfn"%A"
list2|>printfn"%A"

type PersonalName = {FirstName:string; LastName:string}

let john = {FirstName="John"; LastName="Doe"}
let alice = {john with FirstName="Alice"}

printfn"%A"john
printfn"%A"alice