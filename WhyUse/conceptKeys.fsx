let square x = x*x

let squareClone = square
let result=[1..10]|>List.map squareClone

let execFunction aFunc aParam = aFunc aParam

let result2 = execFunction square 12

printfn $"{result} {result2}"

let resExpression = if true then 42 else if false then 24 else 10

if true then printfn "%d"resExpression

type IntAndBool = {intPart: int; boolPart: bool}

let x = {intPart=15; boolPart=false}

printfn $"{x} {x.intPart}"

type IntOrBool = 
  |IntChoice of int
  |BoolChoice of bool

let y = IntChoice 42
let z = BoolChoice true

printfn "%A %A" y z


match x.boolPart with
  |true->printfn "True"
  |false->printfn"False"


let myList = [1;2;3;4;5]

match myList with
  |[]->printfn "Empty"
  |first::second::rest->printfn "First - %d; Second - %d; Rest - %A" first second rest
  |_->printfn "None"


type Shape =
  |Circle of radius:int
  |Rectangle of height:int*width:int
  |Point of int*int
  |Polygon of PointList:(int*int) list

let draw shape = 
  match shape with
  |Circle radius->printfn "The circle has a radius of %d" radius
  |Rectangle (height,width)->printfn"The rectangle is %d high by %d wide" height width
  |Polygon points->printfn"The polygon is made of these points %A" points
  |_->printfn"I don't recognize this shape"

let circle=Circle 10
let rect=Rectangle (4,5)
let point=Point(1,2)
let polygon=Polygon[1,2;2,3;3,7]

[circle;rect;polygon;point]|>List.iter draw