let addingCalculator input = input+1

let logginCalculator innerCalculator input = 
    printfn"input is %A" input
    let res = innerCalculator input
    printfn"result is %A" res
    res

let f
    a//:int
    (b:int )
    (c:int)
    = a+b+c|>float

f 1 2 3|>printfn"%A"



let add1 input = input + 1
let times2 input = input*2

let genericLogger anyFunc i = 
    printfn"input is %A"i 
    let res=anyFunc i 
    printfn"result is %A"res
    res

let add1WithLogging = genericLogger add1
let times2WithLogging = genericLogger times2

add1WithLogging 3

[1..5]|>List.map times2WithLogging


let genericTimer anyFunc input =
    let stopWatch = System.Diagnostics.Stopwatch()
    stopWatch.Start()
    let res = anyFunc input
    printfn"elapsed ms is %A"stopWatch.ElapsedMilliseconds
    res

let add1WithTimer = genericTimer add1WithLogging

add1WithTimer 99


type Animal(noiseMakingStrategy) =
    member this.MakeNoise =
        noiseMakingStrategy() |>printfn"Making noise %A"

let meowing()="Meow"
let cat = Animal(meowing)

cat.MakeNoise

let woofOrBark() = if System.DateTime.Now.Second%2=0
                    then "Woof" else "Bark"
let dog = Animal(woofOrBark)
dog.MakeNoise
dog.MakeNoise