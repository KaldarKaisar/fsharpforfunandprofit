let firstPart, secondPart, _ = (1,2,3)

let elem1::elem2::rest = [1..10]

printfn "%A" firstPart
printfn "%A" rest

let listMatcher aList =
    match aList with
    |[]->printfn "the list is empty"
    |[firstElement]->printfn "the list has an element %A" firstElement
    |[firstElement;secondElement]->printfn "the list has two elements %A %A" firstElement secondElement
    |_->printfn "the list has many elements"

listMatcher []
listMatcher [1]
listMatcher [1;2]
listMatcher [1;2;3;4]


type Address = {Street:string; City:string}
type Customer = {ID:int; Name:string; Address:Address}

let customer1 = {ID=1; Name="Bob";
    Address={Street="Omirova"; City="Apple"}}

let {Name=name1} = customer1
printfn"%A" name1

let {ID=id2; Name=name2} = customer1
printfn"%d %s" id2 name2

let {Name=name3; Address=address3} = customer1
printfn"%s %A" name3 address3

let {Name=name4; Address={Street=street4}} = customer1
printfn "%s %s" name4 street4