open System

let userTimerWithCallback =
    let event = new System.Threading.AutoResetEvent(false)
    let timer = new System.Timers.Timer(2000.0)
    timer.Elapsed.Add(fun _->event.Set()|>ignore)
    printfn"Start in: %O"DateTime.Now.TimeOfDay
    timer.Start()
    printfn"Do something while wait"
    event.WaitOne()|>ignore
    printfn"End in: %O"DateTime.Now.TimeOfDay

let userTimerWithAsync =
    let timer = new System.Timers.Timer(2000.0)
    let timerEvent = Async.AwaitEvent(timer.Elapsed)|>Async.Ignore
    printfn"Start in: %O"DateTime.Now.TimeOfDay
    timer.Start()
    printfn"Do something while wait"
    Async.RunSynchronously timerEvent
    printfn"End in: %O"DateTime.Now.TimeOfDay


let fileWriteWithAsync =
    use stream = new System.IO.FileStream("text.txt",System.IO.FileMode.Create)
    printfn"Start async write"
    let asyncResult = stream.BeginWrite(Array.Empty(),0,0,null,null)
    let async = Async.AwaitIAsyncResult(asyncResult)|>Async.Ignore
    printfn"Do something else"
    Async.RunSynchronously async
    printfn"Async write complete"

let sleepWorkFlow = async{
    printfn"Start sleep workflow at %O"DateTime.Now.TimeOfDay
    do!Async.Sleep 2000
    printfn"End sleep workflow at %O"DateTime.Now.TimeOfDay
}
Async.RunSynchronously sleepWorkFlow


let nestedWorkFlow = async{
    printfn"Start parent"
    let! childWorkflow = Async.StartChild sleepWorkFlow
    do! Async.Sleep 100
    printfn"Do else"
    let!res = childWorkflow

    printfn"Finish parent"
}

Async.RunSynchronously nestedWorkFlow


let testLoop = async{
    for i=10 to 100 do
        printfn"%i before..." i

        do!Async.Sleep 100
        printfn"..after"
}

Async.RunSynchronously testLoop




use cancellationSource = new System.Threading.CancellationTokenSource()

Async.Start(testLoop, cancellationSource.Token)

System.Threading.Thread.Sleep(1000)
cancellationSource.Cancel()


let sleepWorkflowMs ms = async{
    printfn"%i ms workflow started" ms
    do!Async.Sleep ms
    printfn"%i ms workflow ended" ms
}

let workflowInSeries = async{
    let! sleep1 = sleepWorkflowMs 1000
    printfn"Finished one"
    let! sleep2 = sleepWorkflowMs 2000
    printfn"Finished two"
}
#time
Async.RunSynchronously workflowInSeries
#time


let sleep1 = sleepWorkflowMs 1000
let sleep2 = sleepWorkflowMs 2000

#time
[sleep1;sleep2]|>Async.Parallel|>Async.RunSynchronously
#time


let fetchUrl url =
    let req = System.Net.WebRequest.Create(Uri(url))
    use resp = req.GetResponse()
    use stream = resp.GetResponseStream()
    use reader = new IO.StreamReader(stream)
    let html = reader.ReadToEnd()
    printfn"finished downloading %s" url

let sites = ["http://www.bing.com"
             "http://www.google.com"
             //"http://www.microsoft.com"
             "http://www.amazon.com"
             "http://www.yahoo.com"
             "http://www.ya.ru"
             //"fsfadf"
             ]
#time
sites|>List.map fetchUrl
#time

let fetchUrlAsync url = async{
    let req = System.Net.WebRequest.Create(Uri(url))
    use!resp = req.AsyncGetResponse()
    use stream = resp.GetResponseStream()
    use reader = new IO.StreamReader(stream)
    let html = reader.ReadToEnd()
    printfn"finished async downloading %s" url
}

#time
sites|>List.map fetchUrlAsync|>Async.Parallel|>Async.RunSynchronously
#time