let (|Int|_|) str =
    match System.Int32.TryParse(str:string) with
    |(true,int)->Some(int)
    |_->None

let (|Bool|_|) str =
    match System.Boolean.TryParse(str:string) with
    |(true,bool)->Some(bool)
    |_->None

let testParse str =
    match str with
    |Int i->printfn"Int %i" i
    |Bool b->printfn"Bool %b" b
    |_->printfn"Value %s is something else" str

testParse "12"
testParse "true"
testParse "abs"


open System.Text.RegularExpressions
let (|FirstRegexGroup|_|) pattern input =
    let m = Regex.Match(input,pattern)
    if(m.Success)then Some m.Groups.[1].Value else None

let textRegex str =
    match str with
    |FirstRegexGroup "http://(.*?)/(.*)"host->
        printfn"Value is url and host is %s" host
    |FirstRegexGroup ".*?@(.*)" host->
        printfn"Value is email and host is %s" host
    |_->printfn"Value %s is something else" str

textRegex "http://google.com/test"
textRegex "kaldarkajsar@gmail.com"

let (|MultOf3|_|) i = if i%3=0 then Some MultOf3 else None
let (|MultOf5|_|) i = if i%5=0 then Some MultOf5 else None

let fizzBuzz i =
    match i with
    |MultOf3 & MultOf5->printfn"FizzBuzz"
    |MultOf3->printfn"Fizz"
    |MultOf5->printfn"Buzz"
    |_->printfn"%i" i


[1..20]|>List.iter fizzBuzz