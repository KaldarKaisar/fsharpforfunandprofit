let product n =
    let initialValue = 1
    let action productSoFar x = productSoFar * x
    [1..n]|>List.fold action initialValue

printfn "%A" <|product 10


let sumOfOdds n =
    let initialValue = 0
    let action sumOfFar x = if x%2=0 then sumOfFar else sumOfFar+x
    [1..n]|>List.fold action initialValue

printfn "%A" <|sumOfOdds 10


let alternatingSum n =
    let initialValue = (true,0)
    let action (isNeg,sumOfFar) x = if isNeg then (false,sumOfFar-x)
                                                else(true,sumOfFar+x)
    [1..n]|>List.fold action initialValue|>snd

printfn "%A" <|alternatingSum 100


let pattern n initialValue action =
    [1..n]|>List.fold action initialValue


let sumOfSquaresWithFold n =
    let initialValue = 0
    let action sumSoFar x = sumSoFar + (x*x)
    [1..n]|>List.fold action initialValue

printfn "%A" <|sumOfSquaresWithFold 5



type NameAndSize = {Name:string; Size:int}

let maxNameAndSize list =
    let innerMaxNameAndSize initialValue rest =
        let action maxSoFar x = if maxSoFar.Size<x.Size then x else maxSoFar
        rest|>List.fold action initialValue

    match list with
    |[]->None
    |first::rest->
        let max = innerMaxNameAndSize first rest
        Some max

let list = [
    {Name="Alice"; Size=10}
    {Name="Bob"; Size=5}
    {Name="Carol"; Size=22}
    {Name="David"; Size=44}
    {Name="John"; Size=33}
    {Name="Carl"; Size=55}
]

printfn "%A"<|(maxNameAndSize list,maxNameAndSize [])

list|>List.maxBy(fun item->item.Size)|>printfn"%A"
// []|>List.maxBy(fun item->item.Size)|>printfn"%A"
let maxNameAndSize2 list =
    match list with
    |[]->printfn"None"
    |_->list|>List.maxBy(fun item->item.Size)|>printfn"%A"

maxNameAndSize2 list


let alist = [1..25]
match alist with
|first::second::third::rest->printfn"%A"(first,second,third,rest)
|_->printfn"None"