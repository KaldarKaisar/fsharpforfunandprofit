let add3 x = x+3
let mult2 x = x*2
let square x = x*x

[1..10]|>List.map add3 |>printfn"%A"
[1..10]|>List.map mult2 |>printfn"%A"
[1..10]|>List.map square |>printfn"%A"

let add3ThenMult2 x = x|>add3|>mult2
let mult2ThenSquare x = square(mult2 x)

[1..10]|>List.map add3ThenMult2|>printfn"%A"
[1..10]|>List.map mult2ThenSquare|>printfn"%A"

let logMsg msg x= printf"%s%i"msg x;x
let logMsgN msg x= printfn"%s%i"msg x;x

let mult2SquareLogged =
    logMsg "before = "
    >>mult2
    >>logMsg" after mult2="
    >>square
    >>logMsgN" result="

mult2SquareLogged 5
[1..10]|>List.map mult2SquareLogged

[1..10]|>List.reduce (+)|>printfn"%A"

let f = (fun x->string x)>>(fun s->s.Length)>>(fun x->x,x+10)>>(fun (a,b)->printfn"A is %d B is %d" a b)
f 5555555



type DataScale = Hour|Hours|Day|Days|Week|Weeks
type DataDirection = Ago|Hence

let getDate interval scale direction =
    let absHours = match scale with
                    |Hour|Hours->1*interval
                    |Day|Days->24*interval
                    |Week|Weeks->24*7*interval
    let signedHours = match direction with
                        |Ago-> -1*absHours
                        |Hence->absHours
    System.DateTime.Now.AddHours(float signedHours)

let example1 = getDate 5 Days Ago
let example2 = getDate 3 Weeks Hence
let example3 = getDate 1 Hour Hence

[example1;example2;example3]|>List.map (printfn"%A")




type FluentShape = {
    label: string
    color: string
    onClick: FluentShape->FluentShape
}

let defaultShape = {label=""; color=""; onClick=fun shape->shape}
let click shape = shape.onClick shape
let display shape =
    printfn "My label=%s and my color=%s" shape.label shape.color
    shape


let setLabel label shape = {shape with FluentShape.label = label}
let setColor color shape = {shape with FluentShape.color = color}
let appendClickAction action shape = {shape with FluentShape.onClick = shape.onClick>>action}

let setRedBox = setColor"Red">>setLabel"Box"

let setBlueBox = setRedBox>>setColor"Blue"

let changeColorOnClick color = appendClickAction (setColor color)


//setup some test values
let redBox = defaultShape|>setRedBox
let blueBox = defaultShape|>setBlueBox

//create a shape that changes color when clicked
redBox
    |>display
    |>changeColorOnClick "green"
    |>click
    |>display
blueBox
    |>display
    |>appendClickAction(setLabel "box2" >> setColor "green")
    |>click
    |>display


let rainbow = [
    "red";"orange";"yellow";"green";"blue";"indigo";"violet"
]

let showRainbow = 
    let setColorAndDispley color = setColor color >> display
    rainbow
    |>List.map setColorAndDispley
    |>List.reduce (>>)

defaultShape |> showRainbow
printfn $"{nameof(rainbow)}"