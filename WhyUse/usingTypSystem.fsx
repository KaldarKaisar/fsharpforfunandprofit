type EmailAddress = EmailAddress of string

let sendEmail (EmailAddress email) =
    printfn"sent an email to %s" email

let aliceEmail = EmailAddress "alice@example.com"
sendEmail aliceEmail

// sendEmail "kaldarkajsar@gmail.com" Error

let printingExample =
    printfn "%i" 2
    // printfn "%i" 2.0
    // printfn "%i" "hello"
    printfn "%i" 

    printfn "%s" "hello"
    // printfn "%s" 2
    printfn "%s"
    // printfn "%s" "he" "llo"

    printfn "%i %s" 2 "hello"
    // printfn "%i %s" "hello" 2
    printfn "%i %s"

    0

let printString x = printfn "%s" x
let printInt x = printfn "%i" x

[<Measure>]
type cm


[<Measure>]
type meter

[<Measure>]
type feet = 
    static member toMeter(feet: float<feet>) : float<meter> =
        feet * 0.2<meter/feet>


let meter = 100.0<cm>
let yard = 3.0<feet>

let yardInMeter = feet.toMeter(yard)
printfn"%A"meter
printfn"%A"yard
printfn"%A"yardInMeter
// yard + meter ERROR

[<Measure>]
type GBP
[<Measure>]
type USD

let gbp10 = 10.0<GBP>
let usd10 = 10<USD>
let usd10dot = 10.0<USD>

printfn"%A"(gbp10+gbp10)
// printfn"%A"(gbp10+usd10)
printfn"%A"(usd10+usd10)
printfn"%A"(usd10+usd10+3<_>)
// printfn"%A"((2.0<USD>) +7.0<_> +3.0<_>)
// printfn"%A"(usd10+usd10+3.0<_>)
// printfn"%A"(gbp10+2.0)
printfn"%A"(gbp10+2.0<_>)


[<NoEquality; NoComparison>]
type CustomerAccount = {CustomerAccountId: int}

let x = {CustomerAccountId = 1}

// x = x
x.CustomerAccountId = x.CustomerAccountId
