type State = New|Draft|Published|Inactive|Discontinued
let handleState state = 
    match state with
    |New

    |Draft

    |Published

    // |Inactive
    |Discontinued->()
    // |_->()


let getFileInfo filePath =
    let fi = System.IO.FileInfo(filePath)
    if fi.Exists then Some fi else None

let goodFileName = "good.txt"
let badFileName = "bad.txt"

let goodFileInfo = getFileInfo goodFileName
let badFileInfo = getFileInfo badFileName

printfn"%A" goodFileInfo
printfn"%A" badFileInfo

match goodFileInfo with
    |Some fi->printfn"File %A exists" fi
    |None->printfn"Doesn't exist"

match badFileInfo with
|Some fi->printfn"File %s exists" fi.FullName
|None->printfn"Nope"


let rec movingAverages list = 
    match list with
    |[]->[]
    |x::y::rest->
        let avr = (x+y)/2.0
        avr::movingAverages (y::rest)
    |[_]->[]

movingAverages [1.0..0.1..2.0]



type Result<'a,'b> = 
    |Success of 'a
    |Failure of 'b


// type Result<'a,'b> = 
//     |Success of 'a
//     |Failure of 'b
//     |Indeterminate

// type Result<'a,'b> = 
//     |Success of 'a



type FileErrorReason =
    |FileNotFound of string
    |UnauthorizedAccess of string*System.Exception

let performActionOnFile action filePath =
    try
        use sr = new System.IO.StreamReader(filePath:string)
        let result = action sr
        Success result
    with
        | :? System.IO.FileNotFoundException as ex 
            ->Failure (FileNotFound filePath)
        | :? System.Security.SecurityException as ex
            ->Failure (UnauthorizedAccess (filePath,ex))

printfn"%A"<|performActionOnFile (printfn"%A") "good.txt"
printfn"%A"<|performActionOnFile (printfn"%A") "bad.txt"


let middleLayerDo action filePath =
    let fileResult = performActionOnFile action filePath
    fileResult

let topLayerDo action filePath =
    let fileResult = performActionOnFile action filePath
    fileResult


let printFirstLineOfFile filePath =
    let fileResult = topLayerDo (fun fs->fs.ReadLine()) filePath 
    match fileResult with
    |Success result-> printfn"First line is: %s" result
    |Failure reason->
        match reason with
        |FileNotFound file->printfn"File not found: %s" file
        |UnauthorizedAccess (file,_)->printfn"You don't have access to the file: %s"file


let printLengthOfFile filePath =
    let fileResult =
        topLayerDo (fun fs ->fs.ReadToEnd().Length) filePath

    match fileResult with
    |Success result->printfn"Length is %A" result
    |Failure _->printfn"Not specific error"

let writeSomeText filePath someText =
    use writer = new System.IO.StreamWriter(filePath:string)
    writer.WriteLine(someText:string)


writeSomeText goodFileName "Ahahahah Ehehehe\n I won"


printFirstLineOfFile goodFileName
printLengthOfFile goodFileName
printFirstLineOfFile badFileName
printLengthOfFile badFileName


